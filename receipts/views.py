from django.shortcuts import render, get_object_or_404, redirect
from receipts.models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptCreateForm, ExpenseCategoryForm, AccountForm
from django.contrib.auth.models import User

# Create your views here.


@login_required
def receipt_list(request):
    list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": list,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptCreateForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptCreateForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create.html", context)


## create rest of LIST views (feature 12) ##########################
@login_required()
def category_list(request):
    expenses = ExpenseCategory.objects.filter(owner=request.user)
    context = {"expenses": expenses}
    return render(request, "receipts/expenses.html", context)


@login_required()
def account_list(request):
    account_details = Account.objects.filter(owner=request.user)
    context = {"account_details": account_details}
    return render(request, "receipts/account_detail.html", context)


## create rest of CREATE views (feature 13 and 14) ##########################
@login_required()
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            new_category = form.save(False)
            new_category.owner = request.user
            form.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {"form": form}
    return render(request, "receipts/create_category.html", context)


@login_required()
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            new_account = form.save(False)
            new_account.owner = request.user
            form.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    context = {"form": form}
    return render(request, "receipts/create_account.html", context)
