from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt

# Register your models here.
@admin.register(ExpenseCategory)
class EpenseCategoryAdmin(admin.ModelAdmin):
    ExpenseCategory_display = {
        "name",
        "owner",
    }


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    Account_display = {
        "name",
        "number",
        "owner",
    }


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    Receipt_display = {
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
    }
